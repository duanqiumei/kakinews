package com.icbc.cliens;

import com.icbc.cliens.impl.ArticleClientImpl;
import com.icbc.dto.Article;
import com.icbc.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 文章存储远程调用接口
 * fallback:熔断类
 */
@FeignClient(value = "kakinews-article",fallback = ArticleClientImpl.class)
public interface ArticleClient {


    /**
     * 增加
     * @param article
     */
    @RequestMapping(value = "/article",method= RequestMethod.POST)
    public Result add(@RequestBody Article article);
}
