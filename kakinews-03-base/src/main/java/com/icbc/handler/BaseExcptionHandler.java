package com.icbc.handler;

import com.icbc.common.StatusCode;
import com.icbc.entity.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常类
 */
@ControllerAdvice
public class BaseExcptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result excptionHandler(Exception e){
        return new Result(false, StatusCode.ERROR, "系统异常:"+e);
    }
}
