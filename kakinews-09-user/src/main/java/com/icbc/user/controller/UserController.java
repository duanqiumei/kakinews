package com.icbc.user.controller;

import com.icbc.common.StatusCode;
import com.icbc.entity.PageResult;
import com.icbc.entity.Result;
import com.icbc.user.pojo.Admin;
import com.icbc.user.pojo.User;
import com.icbc.user.service.UserService;
import com.icbc.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * user控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;


	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private JwtUtil jwtUtil;

    @Autowired
    private HttpServletRequest request;

	/**
	 * 用户登录的方法
	 * @return
	 */
	@PostMapping("/login")
	public Result login(@RequestBody User user){
		if(user.getMobile()==null||user.getPassword()==null){
			return new Result(false, StatusCode.USER_PASS_ERROR, "手机号或者密码错误");
		}

        User user1 = userService.login(user);

		if(user1!=null &&encoder.matches(user.getPassword(), user1.getPassword())){
			String token = jwtUtil.createJWT(user1.getId(), user1.getLoginname(), "user");
			HashMap<String, Object> hashMap = new HashMap<>();
			hashMap.put("name", user1.getLoginname());
			hashMap.put("Id",user1.getId());
			hashMap.put("token",token);



			return new Result(true, StatusCode.OK, "用户登录成功",hashMap);
		}

		return new Result(false, StatusCode.USER_PASS_ERROR, "手机号或者密码错误");

	}

	/**
	 * 查询全部数据
	 * @return
	 */
	@RequestMapping(method= RequestMethod.GET)
	public Result findAll(){
		return new Result(true, StatusCode.OK,"查询成功",userService.findAll());
	}
	
	/**
	 * 根据ID查询
	 * @param id ID
	 * @return
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Result findById(@PathVariable String id){
		return new Result(true,StatusCode.OK,"查询成功",userService.findById(id));
	}


	/**
	 * 分页+多条件查询
	 * @param searchMap 查询条件封装
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
	public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
		Page<User> pageList = userService.findSearch(searchMap, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<User>(pageList.getTotalElements(), pageList.getContent()) );
	}

	/**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",userService.findSearch(searchMap));
    }
	
	/**
	 * 增加
	 * @param user
	 */
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody User user  ){
		userService.add(user);

		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param user
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody User user, @PathVariable String id ){
		user.setId(id);
		userService.update(user);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 删除
	 * @param id
	 */
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ){
        String authorization = request.getHeader("Authorization");
        Claims claims =(Claims) request.getAttribute("admin_claims");

        if(claims!=null){
            userService.deleteById(id);
            return new Result(true,StatusCode.OK,"删除成功");
        }

        //没有权限
        return new Result(false,StatusCode.ACCESS_ERROR,"你不是管理员，权限不足");
	}

	/**
	 * 短信发送
	 * @param mobile
	 * @return
	 */
	@PostMapping("/sendsms/{mobile}")
	public Result sendSms(@PathVariable String mobile){
		userService.sendSms(mobile);
		return new Result(true,StatusCode.OK,"短信已发送成功");
	}

	/**
	 * 用户注册
	 * @param code
	 * @return
	 */
	@PostMapping("/register/{code}")
	public Result register(@PathVariable String code,@RequestBody User user) {
		//调用service方法即可
		userService.register(code,user);
		//直接返回成功
		return new Result(true,StatusCode.OK,"恭喜：用户注册成功");
	}
}
