package com.icbc.recruit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.icbc.recruit.pojo.Enterprise;

import java.util.List;

/**
 * enterprise数据访问接口
 * @author Administrator
 *
 */
public interface EnterpriseDao extends JpaRepository<Enterprise,String>,JpaSpecificationExecutor<Enterprise>{

    /**
     * springdatejpa 根据方法名生成sql
     * @param ishot
     * @return
     */
    List<Enterprise> findTop9ByIshot(String ishot);

}
