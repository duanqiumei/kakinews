package com.icbc;

import com.icbc.utils.SmsUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
public class SMSApplication {

    public static void main(String[] args) {
        SpringApplication.run(SMSApplication.class,args);
    }

    @Bean
    public SmsUtil smsUtil(){
        return new SmsUtil();
    }

    @Bean
    public Log getLog(){
        return LogFactory.getLog(SMSApplication.class);
    }
}
