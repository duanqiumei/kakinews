import axios from 'axios'
import {getUser} from '@/utils/auth'
// 创建axios实例
const service = axios.create({
  baseURL: 'http://192.168.207.111:7300/mock/5e363e28170af640ea154d84/kakinews', // api的base_url
  timeout: 15000, // 请求超时时间
  headers: {"Authorization": 'Bearer '+getUser().token}
})

export default service
