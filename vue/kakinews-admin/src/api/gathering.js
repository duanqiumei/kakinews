import request from '@/utils/request'

// 优化代码：与微服务网关同步
const group_name = 'gathering'
const api_name = 'gathering'

export default {
  // 获取活动列表
  getList() {
    return request({
      url: `/${group_name}/${api_name}`,
      method: 'get'
    })
  },
  // 分页查询
  search(page, size, searchMap) {
    return request({
      url: `/${group_name}/${api_name}/search/${page}/${size}`,
      method: 'post',
      data: searchMap //  查询条件
    })
  },
  // 保存活动实体
  save(pojo) {
    return request({
      url: `/${group_name}/${api_name}`,
      method: 'post',
      data: pojo
    })
  },
  // 根据id查询一个对象
  findById(id) {
    return request({
      url: `/${group_name}/${api_name}/${id}`,
      method: 'get'
    })
  },
  // 根据id更新对象
  update(id, pojo) {
    return request({
      url: `/${group_name}/${api_name}/${id}`,
      method: 'put',
      data: pojo
    })
  },
  // 删除
  delete(id) {
    return request({
      url: `/${group_name}/${api_name}/${id}`,
      method: 'delete'
    })
  }
}
