package com.icbc.controller;

import com.icbc.common.StatusCode;
import com.icbc.entity.PageResult;
import com.icbc.entity.Result;
import com.icbc.pojo.Spit;
import com.icbc.service.SpitService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * 吐槽控制器
 * @author Kaki Nakajima
 */
@CrossOrigin
@RestController
@RequestMapping("/spit")
public class SpitController {

    @Autowired
    private SpitService spitService;

    @Autowired
    private RedisTemplate redisTemplate;

    //查询所有(不包含评论)
    @GetMapping
    public Result findAll(){
        List<Spit> res = spitService.findByParentidIsNull();
        return new Result(true, StatusCode.OK, "吐槽查询所有成功",res);
    }

    //查询一个
    @GetMapping("/{id}")
    public Result findById(@PathVariable("id") String id){
        return new Result(true, StatusCode.OK, "吐槽查询一个成功",spitService.findById(id));
    }

    //新增一个
    @PostMapping
    public Result seve(@RequestBody Spit spit){
      spitService.seve(spit);
      return new Result(true, StatusCode.OK, "吐槽新增成功");
    }

    //修改一个
    @PutMapping("/{id}")
    public Result update(@RequestBody Spit spit,@PathVariable("id") String id){
        spit.setId(id);
        spitService.update(spit);
        return new Result(true, StatusCode.OK, "吐槽修改成功");
    }

    //删除一个
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable("id") String id){
        spitService.delete(id);
        return new Result(true, StatusCode.OK, "吐槽删除成功");
    }

    //根据父id查询评论
    @GetMapping("/common/{parentid}/{page}/{size}")
    public Result common(
            @PathVariable("parentid") String parentid,
            @PathVariable("page") int page,
            @PathVariable("size") int size
    ){
        Page<Spit> common = spitService.common(parentid, page, size);
        return new Result(true, StatusCode.OK, "根据父id查询评论成功",new PageResult<Spit>(common.getTotalElements(),common.getContent()));
    }

    /**
     * 吐槽点赞
     * @return
     */
    @PutMapping("/thumbup/{id}")
    public Result thumbup(@PathVariable String id){
        //防止重复点赞
        //模拟用户id
        String userId = "xxx";
        String key = "thumbup_"+userId+id;

        String flag = (String)redisTemplate.opsForValue().get(key);
        if(StringUtils.isNoneBlank(flag)){
            return new Result(true, StatusCode.REPEATE_ERROR, "亲，您已经点过赞喽");
        }

        //如果为空，允许点，并把其id存入缓存(并设置过期时效)
        redisTemplate.opsForValue().set(key, "1", 7, TimeUnit.DAYS);

        spitService.thumbup(id);
        return new Result(true, StatusCode.OK, "吐槽点赞成功");
    }

    //搜索吐槽
    @PostMapping("/spit/search/{page}/{size}")
    public Result search(@PathVariable Integer page,@PathVariable Integer size,@RequestBody Spit spit){
        Page<Spit> search = spitService.search(page,size,spit);

        return new Result(true, StatusCode.OK, "吐槽查询成功",new PageResult<Spit>(search.getTotalElements(),search.getContent()));
    }
}
