package com.icbc.cliens;

import com.icbc.cliens.impl.UserClientImpl;
import com.icbc.dto.User;
import com.icbc.entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用户远程调用接口（伪controller）
 */
@FeignClient(value = "kakinews-user",fallback = UserClientImpl.class)
public interface UserClient {

    /**
     * 增加
     * @param user
     */
    @RequestMapping(value = "/user",method= RequestMethod.POST)
    public Result add(@RequestBody User user  );
}
