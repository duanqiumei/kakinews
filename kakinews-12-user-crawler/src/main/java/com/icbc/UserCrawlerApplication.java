package com.icbc;

import com.icbc.utils.IdWorker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import us.codecraft.webmagic.scheduler.RedisScheduler;

@SpringCloudApplication //微服务启动注解
@EnableFeignClients     //开启远程调用
@EnableScheduling       //开启定时任务
public class UserCrawlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserCrawlerApplication.class, args);
    }

    @Value("${spring.redis.host}")
    private String redisHost;

    //爬虫记录器
    @Bean
    public RedisScheduler scheduler(){
        return new  RedisScheduler(redisHost);
    }

    @Bean
    public IdWorker idWorker(){
        return new IdWorker();
    }
}
