# kakinews

#### 介绍
KAKI新闻

#### 软件架构
软件架构说明

  1.后端整体布局采用springcloud芬奇利2.0.0RC1

  2.前台web 基于vue2.0 采用nuxt服务器渲染

  3.前台admin 使用vueAdmin-template-master脚手架


#### 安装教程

1.  后端推荐使用idea
2.  前台推荐使用vscode 

#### 使用说明

1.  后端使用idea，spring工程即可
2.  前台在使用之前删除package-lock.json，使用npm install  ,然后使用npm run dev,可以先用easy-mock来模拟后台接口

#### 参与贡献

1. Fork 本仓库

2. 提交代码

3. 新建 Pull Request

4. 本项目设计者cfei_net,基于1024平台（私人平台），再次基础上进行改造

5. 注：该项目目前还在开发阶段，尚未进行接口对接，后端服务未做周密优化，只是试验阶段

   所以代码下载到本地后还要进行二度开发


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
