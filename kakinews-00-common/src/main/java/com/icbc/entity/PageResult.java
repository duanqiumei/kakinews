package com.icbc.entity;

import java.util.List;

/**
 * 分页实体
 * @author Kaki Nakajima
 */
public class PageResult<T> {
    private Long total; //总记录数
    private List<T> rows; //当前页数据列表

    /**
     * 无参的构造器
     */
    public PageResult() {
        super();
    }
    /**
     * 有参的构造器
     * @param total 总条数
     * @param rows  数据
     */
    public PageResult(Long total, List<T> rows) {
        super();
        this.total = total;
        this.rows = rows;
    }
    public Long getTotal() {
        return total;
    }
    public void setTotal(Long total) {
        this.total = total;
    }
    public List<T> getRows() {
        return rows;
    }
    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}
