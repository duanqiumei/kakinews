package com.icbc.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icbc.common.StatusCode;
import com.icbc.entity.Result;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.servlet.http.HttpServletResponse;

public class GatewayErrorFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return FilterConstants.ERROR_TYPE;
    }

    @Override
    public int filterOrder() {
        return 10;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //1.捕获异常信息
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletResponse response = currentContext.getResponse();
        //ZuulException: 封装其他zuul过滤器执行过程中发现的异常信息
        ZuulException exception = (ZuulException)currentContext.get("throwable");

        //2.把异常信息以json格式输出给前端
        //2.1 构建错误信息
        Result result = new Result(false, StatusCode.ERROR,"kakinews网关异常："+exception.getMessage());

        //2.2 转换Result为json字符串
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String jsonString = objectMapper.writeValueAsString(result);
            //2.3 把json字符串写回给用户
            response.setContentType("text/json;charset=utf-8");
            response.getWriter().write(jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
